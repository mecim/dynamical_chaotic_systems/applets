#!/bin/bash

# Main page
wget http://deim.urv.cat/~antonio.garijo/oracleJDK/logistic.html

# Applet
wget http://deim.urv.cat/~antonio.garijo/oracleJDK/logisticamain.class

# Needed classes
wget http://deim.urv.cat/~antonio.garijo/oracleJDK/Logisticadynam.class
wget http://deim.urv.cat/~antonio.garijo/oracleJDK/Logisticaparam.class

# To run the applet:
# $ ./appletviewer logistic.html
