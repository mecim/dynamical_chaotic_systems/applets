It is sometimes hard to configure browsers to use applets, and it may be also a security risk. Actually, both Mozilla (https://support.mozilla.org/en-US/kb/use-java-plugin-to-view-interactive-content) and Oracle (https://blogs.oracle.com/java-platform-group/moving-to-a-plugin-free-web) recommend that we move away from the plugin architecture towards a more standard-based, plugin-free paradigm.

I have created this project to allow users in Linux to view the applets in the following site:
http://deim.urv.cat/~antonio.garijo/

The script connects to the website and downloads both the HTML page and the *class* files needed to run the applet. Once everything is downloaded, you only need to run this command (in the case of the logistic map):
```
$ appletviewer logistic.html
```

A new window will open with the applet contained in the page.

The explanation contained in the page is also provided as a PDF file, generated with the *.tex* files also provided.
