#!/bin/bash

# Main page
wget http://deim.urv.cat/~antonio.garijo/oracleJDK/standard.html

# Applet
wget http://deim.urv.cat/~antonio.garijo/oracleJDK/standardmain.class

# Needed classes
wget http://deim.urv.cat/~antonio.garijo/oracleJDK/Standarddynam.class
wget http://deim.urv.cat/~antonio.garijo/oracleJDK/Standardparam.class

# To run the applet:
# $ ./appletviewer standard.html
