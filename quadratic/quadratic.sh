#!/bin/bash

# Main page
wget http://deim.urv.cat/~antonio.garijo/oracleJDK/quadratic.html

# Applet
wget http://deim.urv.cat/~antonio.garijo/oracleJDK/quadraticmain.class

# Needed classes
wget http://deim.urv.cat/~antonio.garijo/oracleJDK/Quadraticdynam.class
wget http://deim.urv.cat/~antonio.garijo/oracleJDK/Quadraticparam.class

# To run the applet:
# $ ./appletviewer quadratic.html
